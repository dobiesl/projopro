define(["app"], function(app) {
    "use strict";
    app.config(["$stateProvider", function($stateProvider) {
        $stateProvider.state("zam", {
            url : "/zam",
            "abstract" : true,
            template : "<div ui:view></div>",
            controller : "ZamCtrl"
        });
        $stateProvider.state("zam.manage", {
            url : "/manage",
            templateUrl : "html/zam/manage.html",
            controller : "ManageZamCtrl",
            data : {
                label : "Zamowienia"
            }
        });
        
        $stateProvider.state("zam.add", {
            url : "/add",
            templateUrl : "html/zam/add.html",
            controller : "AddZamCtrl",
            data : {
                label : "Nowe zamowienie"
            }
        });
    }]);
});