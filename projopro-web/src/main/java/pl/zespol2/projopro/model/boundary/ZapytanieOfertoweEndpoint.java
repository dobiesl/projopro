package pl.zespol2.projopro.model.boundary;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.zespol2.projopro.boundary.BaseEndpoint;
import pl.zespol2.projopro.model.ProduktDoZapytania;
import pl.zespol2.projopro.model.ZapytanieOfertowe;
import pl.zespol2.projopro.model.managers.ProduktManager;
import pl.zespol2.projopro.model.managers.ZapytanieOfertoweManager;

@Path("/zaps")
public class ZapytanieOfertoweEndpoint extends BaseEndpoint {

	@Inject
	ZapytanieOfertoweManager manager;

	@Inject
	ProduktManager prodManager;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		return Response.ok(toJsonArray(manager.getAll(), this::zapToJson))
				.build();
	}

	@GET
	@Path("/{zapid:\\d+}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("zapid") long zapid) {
		return Response.ok(zapToJsonDetails(manager.getById(zapid))).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(JsonObject j) {
		ZapytanieOfertowe ee = manager.create(JsonToZap(j));
		return Response.created(
				uriInfo.getRequestUriBuilder().path("{id}").build(ee.getId()))
				.build();
	}

	@PUT
	@Path("/{entityId:\\d+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("entityId") int entityId, JsonObject j) {
		manager.update(entityId, JsonToZap(j));
		return Response.noContent().build();
	}
	
	

	private JsonObject zapToJson(ZapytanieOfertowe ee) {
		JsonObjectBuilder b = Json.createObjectBuilder().add("id", ee.getId())
				.add("skladajacy", ee.getSkladajacy())
				.add("typDostawy", ee.getTypDostawy())
				.add("terminDostawy", ee.getTerminDostawy().getTime());

		if (ee.getKomentarz() != null) {
			b.add("komentarz", ee.getKomentarz());
		}
		if (ee.getProdukty() == null) {
			b.add("liczbaProduktow", 0);
		} else {
			int prods = 0;
			for (ProduktDoZapytania p : ee.getProdukty()) {
				prods += p.getIlosc();
			}
			b.add("liczbaProduktow", prods);
		}

		return b.build();
	};

	private JsonObject zapToJsonDetails(ZapytanieOfertowe ee) {
		JsonObjectBuilder b = Json.createObjectBuilder().add("id", ee.getId())
				.add("skladajacy", ee.getSkladajacy())
				.add("typDostawy", ee.getTypDostawy())
				.add("telefonSkladajacego", ee.getTelefonSkladajacego())
				.add("terminDostawy", ee.getTerminDostawy().getTime());

		if (ee.getKomentarz() != null) {
			b.add("komentarz", ee.getKomentarz());
		}
		if (ee.getProdukty() != null) {
			JsonArrayBuilder ab = Json.createArrayBuilder();
			for (ProduktDoZapytania o : ee.getProdukty()) {
				JsonObjectBuilder ob = Json.createObjectBuilder()
						.add("id", o.getId()).add("ilosc", o.getIlosc())
						.add("produktId", o.getProdukt().getId())
						.add("produktNazwa", o.getProdukt().getNazwa());
				if (o.getKomentarz() != null) {
					ob.add("komentarz", o.getKomentarz());
				}
				ab.add(ob.build());
			}
			b.add("produkty", ab.build());
		}

		return b.build();
	};

	private ZapytanieOfertowe JsonToZap(JsonObject j) {
		ZapytanieOfertowe zo = new ZapytanieOfertowe();
		if (j.containsKey("id")) {
			zo.setId(j.getJsonNumber("id").longValue());
		}
		if (j.containsKey("komentarz")) {
			zo.setKomentarz(j.getString("komentarz"));
		}
		zo.setSkladajacy(j.getString("skladajacy"));
		zo.setTelefonSkladajacego(j.getString("telefonSkladajacego"));
		zo.setTypDostawy(j.getString("typDostawy"));
		zo.setTerminDostawy(new Date(j.getJsonNumber("terminDostawy")
				.longValue()));

		if (j.containsKey("produkty")) {
			JsonArray ar = j.getJsonArray("produkty");
			List<ProduktDoZapytania> pdz = new ArrayList<ProduktDoZapytania>();
			for (JsonValue jsonValue : ar) {
				if (jsonValue instanceof JsonObject) {
					JsonObject jj = ((JsonObject) jsonValue);
					ProduktDoZapytania p = new ProduktDoZapytania();
					if (jj.containsKey("id")) {
						p.setId(jj.getJsonNumber("id").longValue());
					}
					p.setZapytanie(zo);
					p.setIlosc(Integer.parseInt(jj.getString("ilosc")));
					if (jj.containsKey("komentarz"))
						p.setKomentarz(jj.getString("komentarz"));
					p.setProdukt(prodManager.getById(jj.getJsonNumber(
							"produktId").longValue()));
					pdz.add(p);
				}
			}
			zo.setProdukty(pdz);
		}

		return zo;
	}

}
