package pl.zespol2.projopro.model.managers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityNotFoundException;

import pl.zespol2.projopro.model.ProduktDoZamowienia;
import pl.zespol2.projopro.model.ZamowienieEntity;

@Stateless
public class ZamowienieEntityManager extends BasicManager {

	public ZamowienieEntity getById(long id) {
		ZamowienieEntity ee = entityManager.find(ZamowienieEntity.class, id);
		if (ee == null) {
			throw new EntityNotFoundException();
		}
		return ee;
	}

	public List<ZamowienieEntity> getAll() {
		List<ZamowienieEntity> ee = new ArrayList<ZamowienieEntity>();
		ee = entityManager.createNamedQuery("ZamowienieEntity.findAll",
				ZamowienieEntity.class).getResultList();
		return ee;
	}

	public ZamowienieEntity deleteZamowienieEntity(long id) {
		ZamowienieEntity deleted = getById(id);
		entityManager.remove(deleted);
		return deleted;
	}

	public ZamowienieEntity update(long id, ZamowienieEntity updated) {
		ZamowienieEntity old = getById(id);
		old.setKomentarz(updated.getKomentarz());
		old.setDlaKogo(updated.getDlaKogo());
		old.setTelefonDlaKogo(updated.getTelefonDlaKogo());
		old.setTerminDostawy(updated.getTerminDostawy());
		old.setTypDostawy(updated.getTypDostawy());
		old.setCena(updated.getCena());

		if (updated.getProdukty() != null) {
			for (ProduktDoZamowienia cc : updated.getProdukty()) {
				ProduktDoZamowienia oldCc = findPdo(old.getProdukty(), cc);
				if (oldCc != null) {
					oldCc.setProdukt(cc.getProdukt());
					oldCc.setIlosc(cc.getIlosc());
					oldCc.setKomentarz(updated.getKomentarz());
				} else {
					cc.setZamowienie(old);
					List<ProduktDoZamowienia> pdz = old.getProdukty();
					pdz.add(cc);
					old.setProdukty(pdz);
				}
			}
		}
		if (old.getProdukty() != null) {
			Iterator<ProduktDoZamowienia> i = old.getProdukty().iterator();
			while (i.hasNext()) {
				ProduktDoZamowienia cc = i.next();
				if (findPdo(updated.getProdukty(), cc) == null) {
					entityManager.remove(cc);
					i.remove();
				}
			}
		}

		entityManager.merge(old);

		return old;
	}

	public ZamowienieEntity create(ZamowienieEntity data) {
		if (data.getProdukty() == null)
			entityManager.persist(data);
		else {
			List<ProduktDoZamowienia> p = data.getProdukty();
			data.setProdukty(null);
			entityManager.persist(data);
			for (ProduktDoZamowienia produktDoZamowienia : p) {
				produktDoZamowienia.setZamowienie(data);
			}
			data.setProdukty(p);
			entityManager.merge(data);
		}

		return data;
	}

	private ProduktDoZamowienia findPdo(List<ProduktDoZamowienia> ccl,
			ProduktDoZamowienia cc) {
		if (ccl != null) {
			for (ProduktDoZamowienia c : ccl) {
				if (c.getId() == cc.getId()) {
					return c;
				}
			}
		}
		return null;
	}
}