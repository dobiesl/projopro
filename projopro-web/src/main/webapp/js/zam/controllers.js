define(
		[ "app", "angular", "lodash" ],
		function(app, angular, _) {
			"use strict";
			app.controller("ZamCtrl", [ "$scope", "$state", "$rest", "$modal",
					function($scope, $state, $rest, $modal) {
						function close($close) {
							if (!!$close) {
								$close();
							}
							$state.go("zam.manage", {}, {
								reload : true
							});
						}
					} ]);

			app.controller("ManageZamCtrl", [ "$scope", "$modal", "$rest",
					"cmTableSS", "$state", function($scope, $modal, $rest, cmTableSS, $state) {
						$scope.table = cmTableSS($rest.all("zamowienia"), {

						});

						$scope.add = function() {
							$state.go('zam.add');
						}

					} ]);

			app
					.controller(
							"AddZamCtrl",
							[
									"$scope",
									"$modal",
									"$rest",
									"cmTableSS",
									"$state",
									function($scope, $modal, $rest, cmTableSS,
											$state) {

										$scope.wybrany = {};

										$scope.zapytanie = {};
										$scope.wyprods = [];

										$scope.prods = [ {
											"id" : 1,
											"nazwa" : "jakastam"
										}, {
											"id" : 2,
											"nazwa" : "sda"
										} ];

										$scope.wybranyProd = $scope.prods[0];
										$scope.refreshList = function() {
											$rest.all("products").getList()
													.then(function(result) {
														$scope.prods = result;
													});
										}

										$scope.refreshList();

										$scope.typyDostawy = [ {
											val : "Odbior osobisty"
										}, {
											val : "Kurier"
										} ];

										$scope.today = function() {
											$scope.dt = new Date();
										};
										$scope.today();

										$scope.clear = function() {
											$scope.dt = null;
										};

										// Disable weekend selection
										$scope.disabled = function(date, mode) {
											return (mode === 'day' && (date
													.getDay() === 0 || date
													.getDay() === 6));
										};

										$scope.toggleMin = function() {
											$scope.minDate = $scope.minDate ? null
													: new Date();
										};
										$scope.toggleMin();

										$scope.open = function($event) {
											$event.preventDefault();
											$event.stopPropagation();

											$scope.opened = true;
										};

										$scope.dateOptions = {
											formatYear : 'yy',
											startingDay : 1
										};

										$scope.formats = [ 'dd-MMMM-yyyy',
												'yyyy-MM-dd', 'dd.MM.yyyy',
												'shortDate' ];
										$scope.format = $scope.formats[1];

										$scope.cancel = function() {
											$state.go('zam.manage');
										}

										$scope.dodProd = function() {
											var p = {};
											p = {
												"produktId" : $scope.wybrany.prod,
												"ilosc" : $scope.wybrany.ilosc,
												"komentarz" : $scope.wybrany.kom,
												"produktNazwa" : $scope.prods[$scope.wybrany.prod - 1].nazwa
											};
											// $scope.zprodukty =
											// $scope.zapprod;

											$scope.wyprods.push(p);
										}

										$scope.usun = function(idx) {
											$scope.wyprods.splice(idx, 1);
										}

										$scope.create = function() {
											var endpoint = $rest
													.all("zamowienia");

											var z = {};
											z = {
												"dlaKogo" : $scope.zapytanie.skladajacy,
												"cena" : $scope.zapytanie.cena,
												"typDostawy" : $scope.zapytanie.typDostawy,
												"telefonDlaKogo" : $scope.zapytanie.telefonSkladajacego,
												"terminDostawy" : $scope.zapytanie.terminDostawy
														.getTime(),
												"komentarz" : $scope.zapytanie.komentarz,
												"produkty" : $scope.wyprods
											};
											endpoint
													.post(z)
													.then(
															function(posted) {
																$state
																		.go("zam.manage");
															});

										}

									} ]);

		});
