define(["app"], function(app) {
    "use strict";
    app.config(["$stateProvider", function($stateProvider) {
        $stateProvider.state("products", {
            url : "/products",
            "abstract" : true,
            template : "<div ui:view></div>",
            controller : "ProductCtrl"
        });
        $stateProvider.state("products.manage", {
            url : "/manage",
            templateUrl : "html/products/manage.html",
            controller : "ManageProductsCtrl",
            data : {
                label : "Katalog produktów"
            }
        });
    }]);
});