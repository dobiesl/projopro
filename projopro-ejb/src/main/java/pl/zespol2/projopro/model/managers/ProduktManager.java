package pl.zespol2.projopro.model.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityNotFoundException;

import pl.zespol2.projopro.model.Produkt;

@Stateless
public class ProduktManager extends BasicManager {

	public Produkt getById(long id) {
		Produkt p = entityManager.find(Produkt.class, id);
		if (p == null) {
			throw new EntityNotFoundException();
		}
		return p;
	}

	public List<Produkt> getAll() {
		List<Produkt> p = new ArrayList<Produkt>();
		p = entityManager.createNamedQuery("Produkt.findAll",
				Produkt.class).getResultList();
		return p;
	}
	
}
