package pl.zespol2.projopro.model.boundary;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.zespol2.projopro.boundary.BaseEndpoint;
import pl.zespol2.projopro.model.managers.ProduktManager;

@Path("/products")
public class ProduktEndpoint extends BaseEndpoint {

	@Inject
	ProduktManager manager;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		return Response.ok(manager.getAll()).build();
	}

	@GET
	@Path("/{prodId:\\d+}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("prodId") long prodId) {
		return Response.ok(manager.getById(prodId)).build();
	}
}
