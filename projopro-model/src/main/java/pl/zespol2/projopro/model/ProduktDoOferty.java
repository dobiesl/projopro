package pl.zespol2.projopro.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "produkt_do_oferty")
public class ProduktDoOferty {

	@Id
	@SequenceGenerator(name = "ProduktDoOfertyIdGenerator", sequenceName = "pdo_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProduktDoOfertyIdGenerator")
	private long id;

	@Column(name = "komentarz")
	private String komentarz;

	@Column(name = "ilosc")
	private int ilosc;

	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "id_produktu")
	private Produkt produkt;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_oferty")
	private OfertaEntity oferta;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKomentarz() {
		return komentarz;
	}

	public void setKomentarz(String komentarz) {
		this.komentarz = komentarz;
	}

	public int getIlosc() {
		return ilosc;
	}

	public void setIlosc(int ilosc) {
		this.ilosc = ilosc;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}

	public OfertaEntity getOferta() {
		return oferta;
	}

	public void setOferta(OfertaEntity oferta) {
		this.oferta = oferta;
	}

}
