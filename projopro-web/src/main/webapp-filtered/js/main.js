require
		.config({
			paths : {
				angular : "../webjars/angularjs/${angularjs.release}/angular",
				angularUiRouter : "../webjars/angular-ui-router/${angular-ui-router.release}/angular-ui-router",
				angularUiBootstrap : "../webjars/angular-ui-bootstrap/${angular-ui-bootstrap.release}/ui-bootstrap-tpls",
				angularUiSortable : "../webjars/angular-ui-sortable/${angular-ui-sortable.release}/sortable",
				restangular : "../webjars/restangular/${restangular.release}/restangular",
				ngTable : "../webjars/ng-table/${ng-table.release}/ng-table",
				lodash : "../webjars/lodash/${lodash.release}/lodash",
				ace : "../webjars/ace/${ace.version}/src-noconflict/ace",
				angularUiAce : "../webjars/angular-ui-ace/${angular-ui-ace.release}/ui-ace",
				moment : "../webjars/momentjs/${momentjs.release}/moment",
				jquery : "../webjars/jquery/${jquery.release}/jquery",
				"jquery.ui.core" : "../webjars/jquery-ui/${jquery-ui.release}/ui/jquery.ui.core",
				"jquery.ui.mouse" : "../webjars/jquery-ui/${jquery-ui.release}/ui/jquery.ui.mouse",
				"jquery.ui.sortable" : "../webjars/jquery-ui/${jquery-ui.release}/ui/jquery.ui.sortable",
				"jquery.ui.widget" : "../webjars/jquery-ui/${jquery-ui.release}/ui/jquery.ui.widget",
				jqueryFileUpload : "../webjars/jquery-file-upload/${jquery-file-upload.release}/js/jquery.fileupload",
				jqueryIframeTransport : "../webjars/jquery-file-upload/${jquery-file-upload.release}/js/jquery.iframe-transport",
				angularHttpAuth : "../webjars/angular-http-auth/${angular-http-auth.release}/http-auth-interceptor"
			},
			shim : {
				angular : {
					"deps" : [ "jquery" ],
					"exports" : "angular"
				},
				angularUiRouter : [ "angular" ],
				angularUiBootstrap : [ "angular" ],
				angularUiSortable : [ "angular", "jquery.ui.sortable" ],
				lodash : {
					"exports" : "_"
				},
				restangular : [ "angular", "lodash" ],
				ngTable : [ "angular" ],
				angularUiAce : [ "angular", "ace" ],
				jqueryUi : [ "jquery" ],
				"jquery.ui.core" : [ "jquery" ],
				"jquery.ui.mouse" : [ "jquery.ui.core" ],
				"jquery.ui.sortable" : [ "jquery.ui.mouse", "jquery.ui.widget" ],
				"jquery.ui.widget" : [ "jquery" ],
				jqueryIframeTransport : [ "jquery" ],
				jqueryFileUpload : [ "jqueryIframeTransport" ],
				angularHttpAuth : [ "angular" ]
			}
		});

require([ "./boot" ]);