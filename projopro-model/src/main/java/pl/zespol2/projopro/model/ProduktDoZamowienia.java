package pl.zespol2.projopro.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "produkt_do_zamowienia")
public class ProduktDoZamowienia {

	@Id
	@SequenceGenerator(name = "ProduktDoZamIdGenerator", sequenceName = "pdz_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProduktDoZamIdGenerator")
	private long id;

	@Column(name = "komentarz")
	private String komentarz;

	@Column(name = "ilosc")
	private int ilosc;

	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "id_produktu")
	private Produkt produkt;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_zamowienia")
	private ZamowienieEntity zamowienie;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKomentarz() {
		return komentarz;
	}

	public void setKomentarz(String komentarz) {
		this.komentarz = komentarz;
	}

	public int getIlosc() {
		return ilosc;
	}

	public void setIlosc(int ilosc) {
		this.ilosc = ilosc;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}

	public ZamowienieEntity getZamowienie() {
		return zamowienie;
	}

	public void setZamowienie(ZamowienieEntity zamowienie) {
		this.zamowienie = zamowienie;
	}

	

}
