package pl.zespol2.projopro.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "produkt_do_zapytania")
@NamedQuery(name = "ProduktDoZapytania.findAll", query = "SELECT p FROM ProduktDoZapytania p")
public class ProduktDoZapytania {

	
	@Id
	@SequenceGenerator(name = "ProduktDoZapytaniaIdGenerator", sequenceName = "zop_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProduktDoZapytaniaIdGenerator")
	private long id;
	
	@Column(name = "komentarz")
	private String komentarz;
	
	@Column(name = "ilosc")
	private int ilosc;
	
	@ManyToOne(cascade=CascadeType.DETACH)
	@JoinColumn(name = "id_produktu")
	private Produkt produkt;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name = "id_zapytania")
	private ZapytanieOfertowe zapytanie;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKomentarz() {
		return komentarz;
	}

	public void setKomentarz(String komentarz) {
		this.komentarz = komentarz;
	}

	public int getIlosc() {
		return ilosc;
	}

	public void setIlosc(int ilosc) {
		this.ilosc = ilosc;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}

	public ZapytanieOfertowe getZapytanie() {
		return zapytanie;
	}

	public void setZapytanie(ZapytanieOfertowe zapytanie) {
		this.zapytanie = zapytanie;
	}
	
}
