define("app", [ "angular", "lodash", "angularUiRouter", "angularUiBootstrap",
		"angularUiAce", "angularUiSortable", "restangular", "ngTable",
		"jqueryFileUpload" ], function(angular, _) {
	"use strict";
	var app = angular.module("app", [ "ui.router", "ui.bootstrap", "ui.ace",
			"ui.sortable", "restangular", "ngTable" ]);

	app.config([
			"RestangularProvider",
			function(RestangularProvider) {
				RestangularProvider.setBaseUrl("rest");
				// unwrap paged data, pass total items count
				RestangularProvider.addResponseInterceptor(function(data,
						operation) {
					var extractedData;
					if (operation === "getList" && !!data.elements
							&& angular.isArray(data.elements)
							&& angular.isDefined(data.total)) {
						extractedData = data.elements;
						extractedData.total = data.total;
					} else {
						extractedData = data;
					}
					return extractedData;
				});
			} ]);
	app.factory("$rest", [ "Restangular", function(Restangular) {
		return Restangular;
	} ]);

	app.run([
			"$rootScope",
			"$rest",
			"$state",
			"$log",
			"$modal",
			"config",
			"dict",
			function($rootScope, $rest, $state, $log, $modal, config, dict) {

				$rootScope.$on("$stateChangeSuccess", function(event, toState) {
					$rootScope.showMainPanel = (toState.name !== "root");
					$rootScope.navCollapsed = true;
				});

				$rootScope.$on("$stateChangeError", function(event, toState,
						toParams, fromState, fromParams, error) {
					$log
							.error("stateChangeError: " + fromState.name
									+ " -> " + toState.name + ": "
									+ angular.toJson(error, true));
				});

				$rootScope.state = $state;
				$rootScope.config = config;
				$rootScope.dict = dict;

				$rootScope.alert = function(errorMessage) {
					$modal.open({
						templateUrl : "html/errorPopup.html",
						scope : _.extend($rootScope, {
							errorMessage : errorMessage
						}),
						size : "lg",
						windowClass : "error-modal"
					});
				};
			} ]);
	return app;
});

require([ "angular", "_services/table", "./config", "./routes",
		"./controllers", "products/controllers", "products/routes",
		"zaps/controllers", "zaps/routes", "zam/controllers", "zam/routes",
		"ofe/controllers", "ofe/routes" ], function(angular) {
	"use strict";
	/* global window */
	angular.bootstrap(window.document, [ "app" ]);
});
