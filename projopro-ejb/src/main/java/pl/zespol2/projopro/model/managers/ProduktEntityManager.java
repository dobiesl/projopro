package pl.zespol2.projopro.model.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityNotFoundException;

import pl.zespol2.projopro.model.ProduktEntity;

@Stateless
public class ProduktEntityManager extends BasicManager {

	public ProduktEntity getById(long id) {
		ProduktEntity ee = entityManager.find(ProduktEntity.class, id);
		if (ee == null) {
			throw new EntityNotFoundException();
		}
		return ee;
	}

	public List<ProduktEntity> getAll() {
		List<ProduktEntity> ee = new ArrayList<ProduktEntity>();
		ee = entityManager.createNamedQuery("ProduktEntity.findAll",
				ProduktEntity.class).getResultList();
		return ee;
	}

	public ProduktEntity deleteExampleEntity(long id) {
		ProduktEntity deleted = getById(id);
		entityManager.remove(deleted);
		return deleted;
	}

	public ProduktEntity updateProduktEntity(long id, ProduktEntity updated) {
		ProduktEntity old = getById(id);
		old.setIloscNaSkladzieColumn(updated.getIloscNaSkladzieColumn());
		old.setJednostkaColumn(updated.getJednostkaColumn());
		old.setNazwaColumn(updated.getNazwaColumn());
		old.setOpisColumn(updated.getOpisColumn());
		entityManager.merge(old);
		return old;
	}

	public ProduktEntity createProduktEntity(ProduktEntity data) {
		ProduktEntity produktEntity = new ProduktEntity();
		produktEntity.setIloscNaSkladzieColumn(data.getIloscNaSkladzieColumn());
		produktEntity.setJednostkaColumn(data.getJednostkaColumn());
		produktEntity.setNazwaColumn(data.getNazwaColumn());
		produktEntity.setOpisColumn(data.getOpisColumn());
		entityManager.persist(produktEntity);
		return produktEntity;
	}
}