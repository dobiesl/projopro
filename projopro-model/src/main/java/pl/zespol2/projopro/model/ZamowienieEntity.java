package pl.zespol2.projopro.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "zamowienie")
@NamedQuery(name = "ZamowienieEntity.findAll", query = "SELECT ee FROM ZamowienieEntity ee")
public class ZamowienieEntity {

	@Id
	@SequenceGenerator(name = "ZamowienieEntityIdGenerator", sequenceName = "zamowienie_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ZamowienieEntityIdGenerator")
	private long id;

	@Column(name = "komentarz")
	private String komentarz;

	@Column(name = "dla_kogo")
	private String dlaKogo;

	@Column(name = "typ_dostawy")
	private String typDostawy;

	@Column(name = "cena")
	private double cena;

	@Column(name = "termin_dostawy")
	private Date terminDostawy;

	@Column(name = "telefon_dla_kogo")
	private String telefonDlaKogo;

	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "id_oferty")
	private OfertaEntity oferta;

	@OneToMany(mappedBy = "zamowienie", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<ProduktDoZamowienia> produkty;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKomentarz() {
		return komentarz;
	}

	public void setKomentarz(String komentarz) {
		this.komentarz = komentarz;
	}

	public String getDlaKogo() {
		return dlaKogo;
	}

	public void setDlaKogo(String dlaKogo) {
		this.dlaKogo = dlaKogo;
	}

	public String getTypDostawy() {
		return typDostawy;
	}

	public void setTypDostawy(String typDostawy) {
		this.typDostawy = typDostawy;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public Date getTerminDostawy() {
		return terminDostawy;
	}

	public void setTerminDostawy(Date terminDostawy) {
		this.terminDostawy = terminDostawy;
	}

	public String getTelefonDlaKogo() {
		return telefonDlaKogo;
	}

	public void setTelefonDlaKogo(String telefonDlaKogo) {
		this.telefonDlaKogo = telefonDlaKogo;
	}

	public OfertaEntity getOferta() {
		return oferta;
	}

	public void setOferta(OfertaEntity oferta) {
		this.oferta = oferta;
	}

	public List<ProduktDoZamowienia> getProdukty() {
		return produkty;
	}

	public void setProdukty(List<ProduktDoZamowienia> produkty) {
		this.produkty = produkty;
	}

}
