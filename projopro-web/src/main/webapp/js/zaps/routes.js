define(["app"], function(app) {
    "use strict";
    app.config(["$stateProvider", function($stateProvider) {
        $stateProvider.state("zaps", {
            url : "/zaps",
            "abstract" : true,
            template : "<div ui:view></div>",
            controller : "ZapCtrl"
        });
        $stateProvider.state("zaps.manage", {
            url : "/manage",
            templateUrl : "html/zaps/manage.html",
            controller : "ManageZapsCtrl",
            data : {
                label : "Zapytania ofertowe"
            }
        });
        $stateProvider.state("zaps.add", {
            url : "/add",
            templateUrl : "html/zaps/add.html",
            controller : "AddZapsCtrl",
            data : {
                label : "Nowe zapytanie ofertowe"
            }
        });
    }]);
});