define(["app", "lodash", "moment"], function(app, _, moment) {
    "use strict";

    function dateFilter(data, range) {
        return _.filter(data, function(item) {
            var date = moment(item.createdDate);
            var fromMatched = !range.from ? true : !date.isBefore(moment(range.from), "day");
            var toMatched = !range.to ? true : !date.isAfter(moment(range.to), "day");
            return fromMatched && toMatched;
        });
    }

    function stripDateFilters(filter) {
        return _.foldl(filter, function(acc, val, key) {
            if (!_.isObject(val)) {
                acc[key] = val;
            }
            return acc;
        }, {});
    }

    app.factory("cmTableCS", [
        "$filter",
        "ngTableParams",
        "config",
        function($filter, ngTableParams, config) {
            return function(endpoint, paramsTemplate) {
                /* jshint newcap:false */
                return new ngTableParams(_.extend({
                    count : config.paging.count
                }, paramsTemplate), {
                    counts : config.paging.counts,
                    filterDelay : 250,
                    getData : function($deferred, params) {
                        endpoint.getList({}).then(
                            function(data) {
                                if (_.isFunction(paramsTemplate.transform)) {
                                    _.forEach(data, paramsTemplate.transform);
                                }
                                var filteredData = params.filter() ? $filter("filter")(data,
                                    stripDateFilters(params.filter(), function(flt) {
                                        return !_.isObject(flt);
                                    })) : data;
                                filteredData = !!params.filter().createdDate ? dateFilter(
                                    filteredData, params.filter().createdDate) : filteredData;
                                var orderedData = params.sorting() ? $filter("orderBy")(
                                    filteredData, params.orderBy()) : filteredData;
                                var pagedOrderedData = orderedData.slice((params.page() - 1) *
                                    params.count(), params.page() * params.count());
                                params.total(filteredData.length);
                                $deferred.resolve(pagedOrderedData);
                            });
                    }
                });
            };
        }]);

    function transformFilter(filter) {
        return _.foldl(filter, function(acc, val, key) {
            if (typeof val === "object" && val.hasOwnProperty("from")) {
                if (!!val.from) {
                    acc[key + "From"] = moment(val.from).valueOf();
                }
                if (!!val.to) {
                    acc[key + "To"] = moment(val.to).valueOf();
                }
            } else {
                acc[key] = val;
            }
            return acc;
        }, {});
    }

    app.factory("cmTableSS", ["$filter", "ngTableParams", "config",
        function($filter, ngTableParams, config) {
            return function(endpoint, paramsTemplate) {
                /* jshint newcap:false */
                return new ngTableParams(_.extend({
                    count : config.paging.count
                }, paramsTemplate), {
                    counts : config.paging.counts,
                    filterDelay : 250,
                    getData : function($deferred, params) {
                        endpoint.getList(_.extend({
                            orderBy : params.orderBy().join(),
                            limit : params.count(),
                            offset : (params.page() - 1) * params.count()
                        }, transformFilter(params.filter()))).then(function(data) {
                            if (_.isFunction(paramsTemplate.transform)) {
                                _.forEach(data, paramsTemplate.transform);
                            }
                            params.total(data.total);
                            $deferred.resolve(data);
                        });
                    }
                });
            };
        }]);
});
