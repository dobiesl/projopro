package pl.zespol2.projopro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "produkt")
@NamedQuery(name = "ProduktEntity.findAll", query = "SELECT ee FROM ProduktEntity ee")
public class ProduktEntity {

	@Id
	@SequenceGenerator(name = "ProduktEntityIdGenerator", sequenceName = "produkt_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProduktEntityIdGenerator")
	private long id;

	@Column(name = "opis")
	private String opisColumn;
	
	@Column(name = "nazwa")
	private String nazwaColumn;
	
	@Column(name = "jednostka")
	private String jednostkaColumn;
	
	@Column(name = "ilosc_na_skladzie")
	private int iloscNaSkladzieColumn;

	public String getOpisColumn() {
		return opisColumn;
	}

	public void setOpisColumn(String opisColumn) {
		this.opisColumn = opisColumn;
	}

	public String getNazwaColumn() {
		return nazwaColumn;
	}

	public void setNazwaColumn(String nazwaColumn) {
		this.nazwaColumn = nazwaColumn;
	}

	public String getJednostkaColumn() {
		return jednostkaColumn;
	}

	public void setJednostkaColumn(String jednostkaColumn) {
		this.jednostkaColumn = jednostkaColumn;
	}

	public int getIloscNaSkladzieColumn() {
		return iloscNaSkladzieColumn;
	}

	public void setIloscNaSkladzieColumn(int iloscNaSkladzieColumn) {
		this.iloscNaSkladzieColumn = iloscNaSkladzieColumn;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}