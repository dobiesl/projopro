package pl.zespol2.projopro.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "zapytanie_ofertowe")
@NamedQuery(name = "ZapytanieOfertowe.findAll", query = "SELECT p FROM ZapytanieOfertowe p")
public class ZapytanieOfertowe {

	@Id
	@SequenceGenerator(name = "ZapytanieOfertoweIdGenerator", sequenceName = "zo_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ZapytanieOfertoweIdGenerator")
	private long id;

	@Column(name = "komentarz")
	private String komentarz;

	@Column(name = "typ_dostawy")
	private String typDostawy;

	@Column(name = "telefon_skladajacego")
	private String telefonSkladajacego;

	@Column(name = "skladajacy")
	private String skladajacy;

	@Column(name = "termin_dostawy")
	private Date terminDostawy;
	
	@OneToMany(mappedBy = "zapytanie", cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    private List<ProduktDoZapytania> produkty;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKomentarz() {
		return komentarz;
	}

	public void setKomentarz(String komentarz) {
		this.komentarz = komentarz;
	}

	public String getTypDostawy() {
		return typDostawy;
	}

	public void setTypDostawy(String typDostawy) {
		this.typDostawy = typDostawy;
	}

	public String getTelefonSkladajacego() {
		return telefonSkladajacego;
	}

	public void setTelefonSkladajacego(String telefonSkladajacego) {
		this.telefonSkladajacego = telefonSkladajacego;
	}

	public String getSkladajacy() {
		return skladajacy;
	}

	public void setSkladajacy(String skladajacy) {
		this.skladajacy = skladajacy;
	}

	public Date getTerminDostawy() {
		return terminDostawy;
	}

	public void setTerminDostawy(Date terminDostawy) {
		this.terminDostawy = terminDostawy;
	}

	public List<ProduktDoZapytania> getProdukty() {
		return produkty;
	}

	public void setProdukty(List<ProduktDoZapytania> produkty) {
		this.produkty = produkty;
	}
	
}
