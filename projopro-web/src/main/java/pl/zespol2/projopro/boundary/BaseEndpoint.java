package pl.zespol2.projopro.boundary;

import static javax.json.Json.createArrayBuilder;

import java.util.List;
import java.util.function.Function;

import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

public abstract class BaseEndpoint {

    @Context
    protected UriInfo uriInfo;

    protected <T> JsonArray toJsonArray(List<T> items, Function<T, JsonObject> f) {
        JsonArrayBuilder builder = createArrayBuilder();
        for (T i : items) {
            builder.add(f.apply(i));
        }
        return builder.build();
    }

}
