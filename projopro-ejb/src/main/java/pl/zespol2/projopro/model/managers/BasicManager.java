package pl.zespol2.projopro.model.managers;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

public class BasicManager {

    @PersistenceContext(unitName = "ExampleDS")
    protected EntityManager entityManager;

    @Resource(lookup = "java:jboss/datasources/ExampleDS")
    protected DataSource dataSource;

}
