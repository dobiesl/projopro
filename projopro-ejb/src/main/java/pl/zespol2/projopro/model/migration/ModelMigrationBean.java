package pl.zespol2.projopro.model.migration;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;
import javax.sql.DataSource;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;


@Singleton
@Startup
@Named("ModelMigration")
@TransactionManagement(TransactionManagementType.BEAN)
public class ModelMigrationBean {

    @Resource(mappedName = "java:jboss/datasources/ExampleDS")
    private DataSource dataSource;

    private ResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor(getClass()
        .getClassLoader());

    private ModelMigrationConfig config;

    @PostConstruct
    public void onStartup() throws LiquibaseException {
        config = new ModelMigrationConfig();
        config.setChangeLog("META-INF/model-changelog.xml");
        performUpdate();
    }

    private void performUpdate() throws LiquibaseException {
        Connection c = null;
        Liquibase liquibase = null;
        try {
            c = dataSource.getConnection();
            liquibase = createLiquibase(c);
            liquibase.getDatabase();
            liquibase.update(config.getContexts());
        } catch (SQLException e) {
            throw new DatabaseException(e);
        } catch (LiquibaseException ex) {
            throw ex;
        } finally {
            if (liquibase != null && liquibase.getDatabase() != null) {
                liquibase.getDatabase().close();
            } else if (c != null) {
                try {
                    c.rollback();
                    c.close();
                } catch (SQLException e) {
                    // nothing to do
                }
            }
        }
    }

    private Liquibase createLiquibase(Connection c) throws LiquibaseException {
        Liquibase liquibase = new Liquibase(config.getChangeLog(), resourceAccessor,
            createDatabase(c));
        if (config.getParameters() != null) {
            for (Map.Entry<String, String> entry : config.getParameters().entrySet()) {
                liquibase.setChangeLogParameter(entry.getKey(), entry.getValue());
            }
        }
        if (config.isDropFirst()) {
            liquibase.dropAll();
        }
        return liquibase;
    }

    private Database createDatabase(Connection c) throws DatabaseException {
        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(
            new JdbcConnection(c));
        if (config.getDefaultSchema() != null) {
            database.setDefaultSchemaName(config.getDefaultSchema());
        }
        return database;
    }
}
