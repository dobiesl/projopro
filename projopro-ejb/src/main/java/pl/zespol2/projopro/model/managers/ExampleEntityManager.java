package pl.zespol2.projopro.model.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityNotFoundException;

import pl.zespol2.projopro.model.ExampleEntity;

@Stateless
public class ExampleEntityManager extends BasicManager {

	public ExampleEntity getById(long id) {
		ExampleEntity ee = entityManager.find(ExampleEntity.class, id);
		if (ee == null) {
			throw new EntityNotFoundException();
		}
		return ee;
	}

	public List<ExampleEntity> getAll() {
		List<ExampleEntity> ee = new ArrayList<ExampleEntity>();
		ee = entityManager.createNamedQuery("ExampleEntity.findAll",
				ExampleEntity.class).getResultList();
		return ee;
	}

	public ExampleEntity deleteExampleEntity(long id) {
		ExampleEntity deleted = getById(id);
		entityManager.remove(deleted);
		return deleted;
	}

	public ExampleEntity updateExampleEntity(long id, ExampleEntity updated) {
		ExampleEntity old = getById(id);
		old.setExampleNumberColumn(updated.getExampleNumberColumn());
		old.setExampleTextColumn(updated.getExampleTextColumn());
		entityManager.merge(old);
		return old;
	}

	public ExampleEntity createExampleEntity(ExampleEntity data) {
		ExampleEntity exampleEntity = new ExampleEntity();
		exampleEntity.setExampleNumberColumn(data.getExampleNumberColumn());
		exampleEntity.setExampleTextColumn(data.getExampleTextColumn());
		entityManager.persist(exampleEntity);
		return exampleEntity;
	}

}
