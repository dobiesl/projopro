define(["app", "lodash"], function(app, _) {
    "use strict";
    app.constant("errors", {
        invalidUrl : {
            message : "Invalid URL"
        },
        other : {
            message : "An error occured"
        }
    });

    function codeToStatus(dict, code) {
        var a = _.where(dict, {
            code : code
        });
        return a.length > 0 ? _.first(a).name : undefined;
    }

    var booleanYesNo = [{
        name : "Yes",
        code : true
    }, {
        name : "No",
        code : false
    }];


    app.constant("dict", {
        booleanYesNo : booleanYesNo,
        dateFormat : "yyyy-MM-dd"
    });

    app.run(["$rootScope", "$q", function($rootScope, $q) {
        $rootScope.makeFilter = function(dict) {
            var deferred = $q.defer();
            deferred.resolve(_.map(dict, function(item) {
                return {
                    id : item.code,
                    title : item.name
                };
            }));
            return deferred;
        };
    }]);

    app.constant("config", {
        paging : {
            count : 10,
            counts : [10, 20, 50]
        },
        dateFormat : "yyyy-MM-dd",
        dateTimeFormat : "yyyy-MM-dd HH:mm:ss",
        ace : {
            mode : "sql",
            useWrapMode : true,
            showGutter : true,
            theme : "eclipse",
            onLoad : function(editor) {
                editor.setFontSize("16px");
                editor.setShowPrintMargin(false);
                editor.setDisplayIndentGuides(true);
                editor.setHighlightGutterLine(false);
                editor.focus();
            }
        }
    });

    app.filter("isNotNull", function() {
        return function(item) {
            return item !== null;
        };
    });
});
