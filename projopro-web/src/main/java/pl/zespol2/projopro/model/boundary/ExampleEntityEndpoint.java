package pl.zespol2.projopro.model.boundary;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.zespol2.projopro.boundary.BaseEndpoint;
import pl.zespol2.projopro.model.ExampleEntity;
import pl.zespol2.projopro.model.managers.ExampleEntityManager;

@Path("/examples")
public class ExampleEntityEndpoint extends BaseEndpoint {

	@Inject
	ExampleEntityManager manager;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		return Response.ok(
				toJsonArray(manager.getAll(), this::exampleEntityToJson))
				.build();
	}

	@GET
	@Path("/{entityId:\\d+}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("entityId") long entityId) {
		return Response.ok(exampleEntityToJson(manager.getById(entityId)))
				.build();
	}

	@PUT
	@Path("/{entityId:\\d+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("entityId") int entityId, JsonObject j) {
		manager.updateExampleEntity(entityId, jsonToExampleEntity(j));
		return Response.noContent().build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(JsonObject j) {
		ExampleEntity ee = manager.createExampleEntity(jsonToExampleEntity(j));
		return Response.created(
				uriInfo.getRequestUriBuilder().path("{id}").build(ee.getId()))
				.build();
	}

	@DELETE
	@Path("/{entityId:\\d+}")
	public Response delete(@PathParam("entityId") int entityId) {
		manager.deleteExampleEntity(entityId);
		return Response.noContent().build();
	}

	private JsonObject exampleEntityToJson(ExampleEntity ee) {
		JsonObjectBuilder b = Json.createObjectBuilder().add("id", ee.getId())
				.add("exampleNumberColumn", ee.getExampleNumberColumn())
				.add("exampleTextColumn", ee.getExampleTextColumn());
		return b.build();
	};

	private ExampleEntity jsonToExampleEntity(JsonObject j) {
		ExampleEntity ee = new ExampleEntity();
		if (j.containsKey("id")) {
			ee.setId(j.getJsonNumber("id").longValue());
		}
		ee.setExampleNumberColumn(j.getInt("exampleNumberColumn"));
		ee.setExampleTextColumn(j.getString("exampleTextColumn"));
		return ee;
	}
}
