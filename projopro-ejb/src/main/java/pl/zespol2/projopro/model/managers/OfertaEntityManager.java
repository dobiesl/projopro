package pl.zespol2.projopro.model.managers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityNotFoundException;

import pl.zespol2.projopro.model.OfertaEntity;
import pl.zespol2.projopro.model.ProduktDoOferty;

@Stateless
public class OfertaEntityManager extends BasicManager {

	public OfertaEntity getById(long id) {
		OfertaEntity ee = entityManager.find(OfertaEntity.class, id);
		if (ee == null) {
			throw new EntityNotFoundException();
		}
		return ee;
	}

	public List<OfertaEntity> getAll() {
		List<OfertaEntity> ee = new ArrayList<OfertaEntity>();
		ee = entityManager.createNamedQuery("OfertaEntity.findAll",
				OfertaEntity.class).getResultList();
		return ee;
	}

	public OfertaEntity deleteOfertaEntity(long id) {
		OfertaEntity deleted = getById(id);
		entityManager.remove(deleted);
		return deleted;
	}

	public OfertaEntity updateOfertaEntity(long id, OfertaEntity updated) {
		OfertaEntity old = getById(id);
		old.setKomentarz(updated.getKomentarz());
		old.setDlaKogo(updated.getDlaKogo());
		old.setTelefonDlaKogo(updated.getTelefonDlaKogo());
		old.setTerminDostawy(updated.getTerminDostawy());
		old.setTypDostawy(updated.getTypDostawy());
		old.setCena(updated.getCena());
		
		if (updated.getProdukty() != null) {
            for (ProduktDoOferty cc : updated.getProdukty()) {
            	ProduktDoOferty oldCc = findPdo(old.getProdukty(), cc);
                if (oldCc != null) {
                	oldCc.setProdukt(cc.getProdukt());
                	oldCc.setIlosc(cc.getIlosc());
                	oldCc.setKomentarz(updated.getKomentarz());
                } else {
                    cc.setOferta(old);
                    List<ProduktDoOferty> pdz = old.getProdukty();
                    pdz.add(cc);
                    old.setProdukty(pdz);
                }
            }
        }
        if (old.getProdukty() != null) {
            Iterator<ProduktDoOferty> i = old.getProdukty().iterator();
            while (i.hasNext()) {
            	ProduktDoOferty cc = i.next();
                if (findPdo(updated.getProdukty(), cc) == null) {
                    entityManager.remove(cc);
                    i.remove();
                }
            }
        }
        
        entityManager.merge(old);

		return old;
	}

	public OfertaEntity createOfertaEntity(OfertaEntity data) {
		if (data.getProdukty() == null)
			entityManager.persist(data);
		else {
			List<ProduktDoOferty> p = data.getProdukty();
			data.setProdukty(null);
			entityManager.persist(data);
			for (ProduktDoOferty produktDoOferty : p) {
				produktDoOferty.setOferta(data);
			}
			data.setProdukty(p);
			entityManager.merge(data);
		}

		return data;
	}
	
	
	private ProduktDoOferty findPdo(List<ProduktDoOferty> ccl,
			ProduktDoOferty cc) {
		if (ccl != null) {
			for (ProduktDoOferty c : ccl) {
				if (c.getId() == cc.getId()) {
					return c;
				}
			}
		}
		return null;
	}
	
}