define(["app", "lodash"], function(app, _) {
    "use strict";

    app.controller("ErrorCtrl", ["errors", "$stateParams", "$scope",
        function(errors, $stateParams, $scope) {
            if (!!$stateParams.code && !!errors[$stateParams.code]) {
                $scope.errorMessage = errors[$stateParams.code].message.split("\n");
            } else {
                $scope.errorMessage = [errors.other.message];
            }
        }]);
    app.controller("DateFilterCtrl", ["$scope", "$modal", "datePicker",
        function($scope, $modal, datePicker) {
            $scope.date = {
                from : null,
                to : null
            };
            $scope.$watch("params.filter()[name]", function(data) {
                $scope.active = _.isObject(data);
                if (!$scope.active) {
                    $scope.date.from = null;
                    $scope.date.to = null;
                }
            });
            $scope.openModal = function() {
                datePicker.pickDateRange($scope).then(function(data) {
                    if (!!data) {
                        $scope.params.filter()[$scope.name] = _.clone(data);
                    }
                });
            };
        }]);
});