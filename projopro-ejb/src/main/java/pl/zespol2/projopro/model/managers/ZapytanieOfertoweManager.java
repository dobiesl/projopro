package pl.zespol2.projopro.model.managers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityNotFoundException;

import pl.zespol2.projopro.model.ProduktDoZapytania;
import pl.zespol2.projopro.model.ZapytanieOfertowe;

@Stateless
public class ZapytanieOfertoweManager extends BasicManager {

	public ZapytanieOfertowe getById(long id) {
		ZapytanieOfertowe ee = entityManager.find(ZapytanieOfertowe.class, id);
		if (ee == null) {
			throw new EntityNotFoundException();
		}
		return ee;
	}

	public List<ZapytanieOfertowe> getAll() {
		List<ZapytanieOfertowe> ee = new ArrayList<ZapytanieOfertowe>();
		ee = entityManager.createNamedQuery("ZapytanieOfertowe.findAll",
				ZapytanieOfertowe.class).getResultList();
		return ee;
	}

	public void delete(long id) {
		entityManager.remove(getById(id));
	}

	public ZapytanieOfertowe create(ZapytanieOfertowe zo) {
		if (zo.getProdukty() == null)
			entityManager.persist(zo);
		else {
			List<ProduktDoZapytania> p = zo.getProdukty();
			zo.setProdukty(null);
			entityManager.persist(zo);
			for (ProduktDoZapytania produktDoZapytania : p) {
				produktDoZapytania.setZapytanie(zo);
			}
			zo.setProdukty(p);
			entityManager.merge(zo);
		}

		return zo;
	}

	public ZapytanieOfertowe update(long id, ZapytanieOfertowe updated) {
		ZapytanieOfertowe old = getById(id);
		old.setKomentarz(updated.getKomentarz());
		old.setSkladajacy(updated.getSkladajacy());
		old.setTelefonSkladajacego(updated.getTelefonSkladajacego());
		old.setTerminDostawy(updated.getTerminDostawy());
		old.setTypDostawy(updated.getTypDostawy());
		
		if (updated.getProdukty() != null) {
            for (ProduktDoZapytania cc : updated.getProdukty()) {
            	ProduktDoZapytania oldCc = findZap(old.getProdukty(), cc);
                if (oldCc != null) {
                	oldCc.setProdukt(cc.getProdukt());
                	oldCc.setIlosc(cc.getIlosc());
                	oldCc.setKomentarz(updated.getKomentarz());
                } else {
                    cc.setZapytanie(old);
                    //entityManager.persist(cc);
                    List<ProduktDoZapytania> pdz = old.getProdukty();
                    pdz.add(cc);
                    old.setProdukty(pdz);
                }
            }
        }
        if (old.getProdukty() != null) {
            Iterator<ProduktDoZapytania> i = old.getProdukty().iterator();
            while (i.hasNext()) {
            	ProduktDoZapytania cc = i.next();
                if (findZap(updated.getProdukty(), cc) == null) {
                    entityManager.remove(cc);
                    i.remove();
                }
            }
        }
        
        entityManager.merge(old);

		return old;
	}

	private ProduktDoZapytania findZap(List<ProduktDoZapytania> ccl,
			ProduktDoZapytania cc) {
		if (ccl != null) {
			for (ProduktDoZapytania c : ccl) {
				if (c.getId() == cc.getId()) {
					return c;
				}
			}
		}
		return null;
	}

}
