package pl.zespol2.projopro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "produkt")
@NamedQuery(name = "Produkt.findAll", query = "SELECT p FROM Produkt p")
public class Produkt {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "opis")
	private String opis;

	@Column(name = "nazwa")
	private String nazwa;

	@Column(name = "jednostka")
	private String jednostka;

	@Column(name = "ilosc_na_skladzie")
	private int iloscNaSkladzie;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getJednostka() {
		return jednostka;
	}

	public void setJednostka(String jednostka) {
		this.jednostka = jednostka;
	}

	public int getIloscNaSkladzie() {
		return iloscNaSkladzie;
	}

	public void setIloscNaSkladzie(int iloscNaSkladzie) {
		this.iloscNaSkladzie = iloscNaSkladzie;
	}

}
