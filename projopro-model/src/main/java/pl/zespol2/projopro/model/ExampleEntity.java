package pl.zespol2.projopro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "example_table")
@NamedQuery(name = "ExampleEntity.findAll", query = "SELECT ee FROM ExampleEntity ee")
public class ExampleEntity {

	@Id
	@SequenceGenerator(name = "ExampleEntityIdGenerator", sequenceName = "example_table_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ExampleEntityIdGenerator")
	private long id;

	@Column(name = "example_text_column")
	private String exampleTextColumn;

	@Column(name = "example_number_column")
	private int exampleNumberColumn;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getExampleTextColumn() {
		return exampleTextColumn;
	}

	public void setExampleTextColumn(String exampleTextColumn) {
		this.exampleTextColumn = exampleTextColumn;
	}

	public int getExampleNumberColumn() {
		return exampleNumberColumn;
	}

	public void setExampleNumberColumn(int exampleNumberColumn) {
		this.exampleNumberColumn = exampleNumberColumn;
	}

}
