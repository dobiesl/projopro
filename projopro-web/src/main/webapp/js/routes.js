define(["app"], function(app) {
    "use strict";
    app.config(["$stateProvider", "$urlRouterProvider",
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state("root", {
                url : "",
                templateUrl : "html/wait.html"
            });
            $stateProvider.state("error", {
                url : "/error/:code",
                templateUrl : "html/error.html",
                controller : "ErrorCtrl"
            });
            $urlRouterProvider.otherwise("/error/invalidUrl");
        }]);
});