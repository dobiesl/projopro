define(["app"], function(app) {
    "use strict";
    app.config(["$stateProvider", function($stateProvider) {
        $stateProvider.state("ofe", {
            url : "/ofe",
            "abstract" : true,
            template : "<div ui:view></div>",
            controller : "OfeCtrl"
        });
        $stateProvider.state("ofe.manage", {
            url : "/manage",
            templateUrl : "html/ofe/manage.html",
            controller : "ManageOfeCtrl",
            data : {
                label : "Oferty"
            }
        });
        $stateProvider.state("ofe.add", {
            url : "/add",
            templateUrl : "html/ofe/add.html",
            controller : "AddOfeCtrl",
            data : {
                label : "Nowa oferta"
            }
        });
    }]);
});