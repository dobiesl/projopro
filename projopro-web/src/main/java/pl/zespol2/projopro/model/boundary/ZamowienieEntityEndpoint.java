package pl.zespol2.projopro.model.boundary;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.zespol2.projopro.boundary.BaseEndpoint;
import pl.zespol2.projopro.model.ProduktDoZamowienia;
import pl.zespol2.projopro.model.ZamowienieEntity;
import pl.zespol2.projopro.model.managers.OfertaEntityManager;
import pl.zespol2.projopro.model.managers.ProduktManager;
import pl.zespol2.projopro.model.managers.ZamowienieEntityManager;

@Path("/zamowienia")
public class ZamowienieEntityEndpoint extends BaseEndpoint {

	@Inject
	ZamowienieEntityManager manager;
	
	@Inject
	OfertaEntityManager ofManager;

	@Inject
	ProduktManager prodManager;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		return Response.ok(
				toJsonArray(manager.getAll(), this::zamowienieEntityToJson))
				.build();
	}

	@GET
	@Path("/{entityId:\\d+}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("entityId") long entityId) {
		return Response.ok(zamowienieEntityToJsonDetails(manager.getById(entityId)))
				.build();
	}

	@PUT
	@Path("/{entityId:\\d+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("entityId") int entityId, JsonObject j) {
		manager.update(entityId, jsonToZamowienieEntity(j));
		return Response.noContent().build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(JsonObject j) {
		ZamowienieEntity ee = manager.create(jsonToZamowienieEntity(j));
		return Response.created(
				uriInfo.getRequestUriBuilder().path("{id}").build(ee.getId()))
				.build();
	}

	@DELETE
	@Path("/{entityId:\\d+}")
	public Response delete(@PathParam("entityId") int entityId) {
		manager.deleteZamowienieEntity(entityId);
		return Response.noContent().build();
	}

	
	private JsonObject zamowienieEntityToJson(ZamowienieEntity ee) {
		JsonObjectBuilder b = Json.createObjectBuilder().add("id", ee.getId())
				.add("dlaKogo", ee.getDlaKogo())
				.add("typDostawy", ee.getTypDostawy())
				.add("terminDostawy", ee.getTerminDostawy().getTime())
				.add("cena", ee.getCena());

		if (ee.getKomentarz() != null) {
			b.add("komentarz", ee.getKomentarz());
		}
		if (ee.getProdukty() == null) {
			b.add("liczbaProduktow", 0);
		} else {
			int prods = 0;
			for (ProduktDoZamowienia p : ee.getProdukty()) {
				prods += p.getIlosc();
			}
			b.add("liczbaProduktow", prods);
		}

		return b.build();
	};

	private JsonObject zamowienieEntityToJsonDetails(ZamowienieEntity ee) {
		JsonObjectBuilder b = Json.createObjectBuilder().add("id", ee.getId())
				.add("dlaKogo", ee.getDlaKogo())
				.add("typDostawy", ee.getTypDostawy())
				.add("telefonDlaKogo", ee.getTelefonDlaKogo())
				.add("cena", ee.getCena())
				.add("terminDostawy", ee.getTerminDostawy().getTime());

		if (ee.getKomentarz() != null) {
			b.add("komentarz", ee.getKomentarz());
		}
		if (ee.getOferta() != null) {
			b.add("idOferty", ee.getOferta().getId());
		}
		if (ee.getProdukty() != null) {
			JsonArrayBuilder ab = Json.createArrayBuilder();
			for (ProduktDoZamowienia o : ee.getProdukty()) {
				JsonObjectBuilder ob = Json.createObjectBuilder()
						.add("id", o.getId()).add("ilosc", o.getIlosc())
						.add("produktId", o.getProdukt().getId())
						.add("produktNazwa", o.getProdukt().getNazwa());
				if (o.getKomentarz() != null) {
					ob.add("komentarz", o.getKomentarz());
				}
				ab.add(ob.build());
			}
			b.add("produkty", ab.build());
		}

		return b.build();
	};

	private ZamowienieEntity jsonToZamowienieEntity(JsonObject j) {
		ZamowienieEntity zo = new ZamowienieEntity();
		if (j.containsKey("id")) {
			zo.setId(j.getJsonNumber("id").longValue());
		}
		if (j.containsKey("komentarz")) {
			zo.setKomentarz(j.getString("komentarz"));
		}
		zo.setDlaKogo(j.getString("dlaKogo"));
		zo.setTelefonDlaKogo(j.getString("telefonDlaKogo"));
		zo.setTypDostawy(j.getString("typDostawy"));
		zo.setTerminDostawy(new Date(j.getJsonNumber("terminDostawy")
				.longValue()));
		if (j.containsKey("zapytanieOfertoweId")) {
			zo.setOferta(ofManager.getById(j.getJsonNumber("ofertaId").longValue()));
		}
		zo.setCena(Double.parseDouble(j.getString("cena")));

		if (j.containsKey("produkty")) {
			JsonArray ar = j.getJsonArray("produkty");
			List<ProduktDoZamowienia> pdz = new ArrayList<ProduktDoZamowienia>();
			for (JsonValue jsonValue : ar) {
				if (jsonValue instanceof JsonObject) {
					JsonObject jj = ((JsonObject) jsonValue);
					ProduktDoZamowienia p = new ProduktDoZamowienia();
					if (jj.containsKey("id")) {
						p.setId(jj.getJsonNumber("id").longValue());
					}
					p.setZamowienie(zo);
					p.setIlosc(Integer.parseInt(jj.getString("ilosc")));
					if (jj.containsKey("komentarz"))
						p.setKomentarz(jj.getString("komentarz"));
					p.setProdukt(prodManager.getById(jj.getJsonNumber(
							"produktId").longValue()));
					pdz.add(p);
				}
			}
			zo.setProdukty(pdz);
		}

		return zo;
	}
}
