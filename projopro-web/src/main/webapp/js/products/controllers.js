define(["app", "angular", "lodash"], function(app, angular, _) {
    "use strict";
    app.controller("ProductCtrl", ["$scope", "$state", "$rest", "$modal", 
        function($scope, $state, $rest, $modal) {
            function close($close) {
                if (!!$close) {
                    $close();
                }
                $state.go("products.manage", {}, {
                    reload : true
                });
            }
        }]);

    app.controller("ManageProductsCtrl", ["$scope", "$modal", "$rest", "cmTableSS",
        function($scope, $modal, $rest, cmTableSS) {
            $scope.table = cmTableSS($rest.all("products"), {
            	
            });
            
           
        }]);
});
