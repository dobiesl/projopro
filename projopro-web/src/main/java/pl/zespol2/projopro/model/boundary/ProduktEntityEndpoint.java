package pl.zespol2.projopro.model.boundary;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.zespol2.projopro.boundary.BaseEndpoint;
import pl.zespol2.projopro.model.ProduktEntity;
import pl.zespol2.projopro.model.managers.ProduktEntityManager;

@Path("/produkty")
public class ProduktEntityEndpoint extends BaseEndpoint {

	@Inject
	ProduktEntityManager manager;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		return Response.ok(
				toJsonArray(manager.getAll(), this::produktEntityToJson))
				.build();
	}

	@GET
	@Path("/{entityId:\\d+}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("entityId") long entityId) {
		return Response.ok(produktEntityToJson(manager.getById(entityId)))
				.build();
	}

	@PUT
	@Path("/{entityId:\\d+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("entityId") int entityId, JsonObject j) {
		manager.updateProduktEntity(entityId, jsonToProduktEntity(j));
		return Response.noContent().build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(JsonObject j) {
		ProduktEntity ee = manager.createProduktEntity(jsonToProduktEntity(j));
		return Response.created(
				uriInfo.getRequestUriBuilder().path("{id}").build(ee.getId()))
				.build();
	}

	@DELETE
	@Path("/{entityId:\\d+}")
	public Response delete(@PathParam("entityId") int entityId) {
		manager.deleteExampleEntity(entityId);
		return Response.noContent().build();
	}

	
	private JsonObject produktEntityToJson(ProduktEntity ee) {
		JsonObjectBuilder b = Json.createObjectBuilder().add("id", ee.getId())
				.add("IloscNaSkladzieColumn", ee.getIloscNaSkladzieColumn())
				.add("JednostkaColumn", ee.getJednostkaColumn())
				.add("NazwaColumn", ee.getNazwaColumn())
				.add("OpisColumn", ee.getOpisColumn());
		
		return b.build();
	};

	private ProduktEntity jsonToProduktEntity(JsonObject j) {
		ProduktEntity ee = new ProduktEntity();
		if (j.containsKey("id")) {
			ee.setId(j.getJsonNumber("id").longValue());
		}
		ee.setIloscNaSkladzieColumn(j.getInt("IloscNaSkladzieColumn"));
		ee.setJednostkaColumn(j.getString("JednostkaColumn"));
		ee.setNazwaColumn(j.getString("NazwaColumn"));
		ee.setOpisColumn(j.getString("OpisColumn"));
		return ee;
	}
}
