package pl.zespol2.projopro.model.boundary;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.zespol2.projopro.boundary.BaseEndpoint;
import pl.zespol2.projopro.model.OfertaEntity;
import pl.zespol2.projopro.model.ProduktDoOferty;
import pl.zespol2.projopro.model.managers.OfertaEntityManager;
import pl.zespol2.projopro.model.managers.ProduktManager;
import pl.zespol2.projopro.model.managers.ZapytanieOfertoweManager;

@Path("/oferty")
public class OfertaEntityEndpoint extends BaseEndpoint {

	@Inject
	OfertaEntityManager manager;
	
	@Inject
	ZapytanieOfertoweManager zapManager;

	@Inject
	ProduktManager prodManager;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		return Response.ok(
				toJsonArray(manager.getAll(), this::ofertaEntityToJson))
				.build();
	}

	@GET
	@Path("/{entityId:\\d+}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("entityId") long entityId) {
		return Response.ok(ofertaEntityToJsonDetails(manager.getById(entityId)))
				.build();
	}

	@PUT
	@Path("/{entityId:\\d+}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("entityId") int entityId, JsonObject j) {
		manager.updateOfertaEntity(entityId, JsonToOferta(j));
		return Response.noContent().build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(JsonObject j) {
		OfertaEntity ee = manager.createOfertaEntity(JsonToOferta(j));
		return Response.created(
				uriInfo.getRequestUriBuilder().path("{id}").build(ee.getId()))
				.build();
	}

	@DELETE
	@Path("/{entityId:\\d+}")
	public Response delete(@PathParam("entityId") int entityId) {
		manager.deleteOfertaEntity(entityId);
		return Response.noContent().build();
	}

	

	private JsonObject ofertaEntityToJson(OfertaEntity ee) {
		JsonObjectBuilder b = Json.createObjectBuilder().add("id", ee.getId())
				.add("dlaKogo", ee.getDlaKogo())
				.add("typDostawy", ee.getTypDostawy())
				.add("terminDostawy", ee.getTerminDostawy().getTime())
				.add("cena", ee.getCena());

		if (ee.getKomentarz() != null) {
			b.add("komentarz", ee.getKomentarz());
		}
		if (ee.getProdukty() == null) {
			b.add("liczbaProduktow", 0);
		} else {
			int prods = 0;
			for (ProduktDoOferty p : ee.getProdukty()) {
				prods += p.getIlosc();
			}
			b.add("liczbaProduktow", prods);
		}

		return b.build();
	};

	private JsonObject ofertaEntityToJsonDetails(OfertaEntity ee) {
		JsonObjectBuilder b = Json.createObjectBuilder().add("id", ee.getId())
				.add("dlaKogo", ee.getDlaKogo())
				.add("typDostawy", ee.getTypDostawy())
				.add("telefonDlaKogo", ee.getTelefonDlaKogo())
				.add("cena", ee.getCena())
				.add("terminDostawy", ee.getTerminDostawy().getTime());

		if (ee.getKomentarz() != null) {
			b.add("komentarz", ee.getKomentarz());
		}
		if (ee.getZapytanieOfertowe() != null) {
			b.add("idZapytaniaOfertowego", ee.getZapytanieOfertowe().getId());
		}
		if (ee.getProdukty() != null) {
			JsonArrayBuilder ab = Json.createArrayBuilder();
			for (ProduktDoOferty o : ee.getProdukty()) {
				JsonObjectBuilder ob = Json.createObjectBuilder()
						.add("id", o.getId()).add("ilosc", o.getIlosc())
						.add("produktId", o.getProdukt().getId())
						.add("produktNazwa", o.getProdukt().getNazwa());
				if (o.getKomentarz() != null) {
					ob.add("komentarz", o.getKomentarz());
				}
				ab.add(ob.build());
			}
			b.add("produkty", ab.build());
		}

		return b.build();
	};

	private OfertaEntity JsonToOferta(JsonObject j) {
		OfertaEntity zo = new OfertaEntity();
		if (j.containsKey("id")) {
			zo.setId(j.getJsonNumber("id").longValue());
		}
		if (j.containsKey("komentarz")) {
			zo.setKomentarz(j.getString("komentarz"));
		}
		zo.setDlaKogo(j.getString("dlaKogo"));
		zo.setTelefonDlaKogo(j.getString("telefonDlaKogo"));
		zo.setTypDostawy(j.getString("typDostawy"));
		zo.setTerminDostawy(new Date(j.getJsonNumber("terminDostawy")
				.longValue()));
		if (j.containsKey("zapytanieOfertoweId")) {
			zo.setZapytanieOfertowe(zapManager.getById(j.getJsonNumber("zapytanieOfertoweId").longValue()));
		}
		zo.setCena(Double.parseDouble( j.getString("cena")));

		if (j.containsKey("produkty")) {
			JsonArray ar = j.getJsonArray("produkty");
			List<ProduktDoOferty> pdz = new ArrayList<ProduktDoOferty>();
			for (JsonValue jsonValue : ar) {
				if (jsonValue instanceof JsonObject) {
					JsonObject jj = ((JsonObject) jsonValue);
					ProduktDoOferty p = new ProduktDoOferty();
					if (jj.containsKey("id")) {
						p.setId(jj.getJsonNumber("id").longValue());
					}
					p.setOferta(zo);
					p.setIlosc(Integer.parseInt( jj.getString("ilosc")));
					if (jj.containsKey("komentarz"))
						p.setKomentarz(jj.getString("komentarz"));
					p.setProdukt(prodManager.getById(jj.getJsonNumber(
							"produktId").longValue()));
					pdz.add(p);
				}
			}
			zo.setProdukty(pdz);
		}

		return zo;
	}
	
	
}
